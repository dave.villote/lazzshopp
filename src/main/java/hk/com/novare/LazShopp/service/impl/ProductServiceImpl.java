package hk.com.novare.LazShopp.service.impl;

import hk.com.novare.LazShopp.entity.Product;
import hk.com.novare.LazShopp.repository.ProductRepository;
import hk.com.novare.LazShopp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    ProductRepository repository;

    @Autowired
    public ProductServiceImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Product> getAllProducts() {
        return repository.findAll();
    }

    @Override
    public Product addNewProduct(Product product) {
        return repository.save(product);
    }

    @Override
    public Product updateProduct(Product product) throws Exception {
        boolean isExisting = repository.findById(product.getId()).isPresent();
        if (isExisting) {
            return repository.save(product);
        } else {
            throw new Exception("Product does not exist!");
        }
    }

    @Override
    public void sellProduct(int id, int quantity) {
        Product product = repository.findById(id).orElseThrow(NullPointerException::new);
        product.setQuantity(product.getQuantity() - quantity);
        repository.save(product);
    }
}
