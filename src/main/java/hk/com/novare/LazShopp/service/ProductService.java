package hk.com.novare.LazShopp.service;

import hk.com.novare.LazShopp.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();

    Product addNewProduct(Product product);

    Product updateProduct(Product product) throws Exception;

    void sellProduct(int id, int quantity);
}
