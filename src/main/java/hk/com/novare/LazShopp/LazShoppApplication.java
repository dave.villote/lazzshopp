package hk.com.novare.LazShopp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazShoppApplication {

	public static void main(String[] args) {
		SpringApplication.run(LazShoppApplication.class, args);
	}

}
